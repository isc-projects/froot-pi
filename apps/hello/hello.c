
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>

static volatile unsigned long count;


//-----------------------------------------------
// Calculate the Collatz conjecture with a
// recursive function
static int collatz(int n) {
	count++;

	if (n <= 1) return 1;

	if (n % 2 == 1) return collatz(3 * n + 1);

	return collatz(n / 2);
}



//-----------------------------------------------
int main(void) {
	printf("Hello World\n");

	count = 0;
	printf("The Collatz Conjecture returned %d", 
		collatz(getpid() + (int) time(NULL)));
	printf(" in %lu loops\n", count);

	return 0;
}

