
# Nard Linux SDK
# http://www.arbetsmyra.dyndns.org/nard
# Copyright (C) 2014-2017 Ronny Nilsson


# Check prerequisites
ifeq ($(PKG_VER),)
$(error Variable PKG_VER hasn't expanded correctly)
else ifeq ($(PKG_VER),$(PKG_NAME))
$(error Variable PKG_VER hasn't expanded correctly)
endif


ifndef $(PKG_NAME)-INST
	CFLAGS := $(CROSS_CFLAGS)
	CFLAGS += -fPIC -rdynamic -Wno-multichar -std=gnu89
	CFLAGS += -D_LARGEFILE_SOURCE -D_LARGEFILE64_SOURCE
	CFLAGS += -D_FILE_OFFSET_BITS=64
	CFLAGS += -I "$(PATH_APPS)/$(PKG_NAME)/$(PKG_VER)"
	CPPFLAGS := $(CROSS_CPPFLAGS)
	CPPFLAGS += -fPIC -D_LARGEFILE_SOURCE -D_LARGEFILE64_SOURCE
	CPPFLAGS += -D_FILE_OFFSET_BITS=64

	$(PKG_NAME)-INST := $(PATH_FS)/usr
endif

