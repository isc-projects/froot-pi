From 5725dd8fa888b4dcdff58241f9d3d3ac42a048e2 Mon Sep 17 00:00:00 2001
From: Wang Nan <wangnan0@huawei.com>
Date: Tue, 24 Nov 2015 13:36:06 +0000
Subject: tools build: Clean CFLAGS and LDFLAGS for fixdep

Sometimes passing variables to tools/build is dangerous. For example, on
my platform there is a gcc problem (gcc 4.8.1):

It passes the stackprotector-all feature check:

  $ gcc -fstack-protector-all -c ./test.c
  $ echo $?
  0

But requires LDFLAGS support if separate compiling and linking:
  $ gcc -fstack-protector-all -c ./test.c
  $ gcc ./test.o
  ./test.o: In function `main':
  test.c:(.text+0xb): undefined reference to `__stack_chk_guard'
  test.c:(.text+0x21): undefined reference to `__stack_chk_guard'
  collect2: error: ld returned 1 exit status
  $ gcc -fstack-protector-all ./test.o
  $ echo $?
  0
  $ gcc ./test.o -lssp
  $ echo $?
  0
  $

In this environment building perf throws an error:

  $ make
    BUILD:   Doing 'make -j24' parallel build
  config/Makefile:344: No libunwind found. Please install libunwind-dev[el] >= 1.1 and/or set LIBUNWIND_DIR
  config/Makefile:403: No libaudit.h found, disables 'trace' tool, please install audit-libs-devel or libaudit-dev
  config/Makefile:418: slang not found, disables TUI support. Please install slang-devel or libslang-dev
  config/Makefile:432: GTK2 not found, disables GTK2 support. Please install gtk2-devel or libgtk2.0-dev
  config/Makefile:564: No bfd.h/libbfd found, please install binutils-dev[el]/zlib-static/libiberty-dev to gain symbol demangling
  config/Makefile:606: No numa.h found, disables 'perf bench numa mem' benchmark, please install numactl-devel/libnuma-devel/libnuma-dev
    CC       fixdep.o
    LD       fixdep-in.o
    LINK     fixdep
  fixdep-in.o: In function `parse_dep_file':
  /kernel/tools/build/fixdep.c:47: undefined reference to `__stack_chk_guard'
  /kernel/tools/build/fixdep.c:117: undefined reference to `__stack_chk_guard'
  fixdep-in.o: In function `main':
  /kernel-hydrogen/tools/build/fixdep.c:156: undefined reference to `__stack_chk_guard'
  /kernel/tools/build/fixdep.c:168: undefined reference to `__stack_chk_guard'
  collect2: error: ld returned 1 exit status
  make[2]: *** [fixdep] Error 1
  make[1]: *** [fixdep] Error 2
  make: *** [all] Error 2

This is because the CFLAGS used in building perf pollutes the CFLAGS
used for fixdep, passing -fstack-protector-all to buiold fixdep which is
obviously not required. Since fixdep is a small host side tool, we
should keep its CFLAGS/LDFLAGS simple and clean.

This patch clears the CFLAGS and LDFLAGS passed when building fixdep, so
such gcc problem won't block the perf build process.

Signed-off-by: Wang Nan <wangnan0@huawei.com>
Acked-by: Jiri Olsa <jolsa@kernel.org>
Cc: Alexei Starovoitov <ast@kernel.org>
Cc: Masami Hiramatsu <masami.hiramatsu.pt@hitachi.com>
Cc: Zefan Li <lizefan@huawei.com>
Cc: pi3orama@163.com
Link: http://lkml.kernel.org/r/1448372181-151723-2-git-send-email-wangnan0@huawei.com
Signed-off-by: Arnaldo Carvalho de Melo <acme@redhat.com>
---
 tools/build/Makefile.include | 2 +-
 1 file changed, 1 insertion(+), 1 deletion(-)

diff --git a/tools/build/Makefile.include b/tools/build/Makefile.include
index 4e09ad617a60..6254760290c9 100644
--- a/tools/build/Makefile.include
+++ b/tools/build/Makefile.include
@@ -4,7 +4,7 @@ ifdef CROSS_COMPILE
 fixdep:
 else
 fixdep:
-	$(Q)$(MAKE) -C $(srctree)/tools/build fixdep
+	$(Q)$(MAKE) -C $(srctree)/tools/build CFLAGS= LDFLAGS= fixdep
 endif
 
 .PHONY: fixdep
-- 
2.11.0

