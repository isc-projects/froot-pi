/*
 *  linux/arch/arm/mach-bcm2709/bcm2708.c
 *
 *  Copyright (C) 2010 Broadcom
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include <linux/init.h>
#include <linux/dma-mapping.h>
#include <linux/interrupt.h>
#include <linux/clk-provider.h>
#include <linux/clocksource.h>
#include <linux/io.h>
#include <linux/module.h>
#include <linux/of_platform.h>

#include <asm/system_info.h>
#include <asm/mach-types.h>
#include <asm/cputype.h>

#include <asm/mach/arch.h>
#include <asm/mach/map.h>

#include <mach/system.h>

#include "bcm27xx.h"


/* command line parameters */
static unsigned boardrev, serial;
static unsigned reboot_part = 0;


static void __init bcm2708_map_io(void)
{
	bcm_map_io(0x20000000ul, 0x40000000ul, 0x0fffffff);
}


static void __init bcm2708_init(void)
{
	bcm27xx_init(boardrev, serial, reboot_part);
}


static const char * const bcm2708_compat[] = {
	"brcm,bcm2708",
	NULL
};


MACHINE_START(BCM2709, "BCM2708")
#ifdef CONFIG_SMP
	.smp = smp_ops(bcm27xx_smp_ops),
#endif
	.map_io = bcm2708_map_io,
	.init_machine = bcm2708_init,
	.init_early = bcm27xx_init_early,
	.reserve = bcm27xx_board_reserve,
	.restart = bcm27xx_restart,
	.dt_compat = bcm2708_compat,
MACHINE_END


module_param(boardrev, uint, 0644);
module_param(serial, uint, 0644);
module_param(reboot_part, uint, 0644);

