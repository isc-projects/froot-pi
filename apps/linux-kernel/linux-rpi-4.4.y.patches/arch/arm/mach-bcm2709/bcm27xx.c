/*
 *  linux/arch/arm/mach-bcm2709/bcm27xx.c
 *
 *  Unified BCM27xx support 2016 Ronny Nilsson
 *  Copyright (C) 2010 Broadcom
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include <linux/init.h>
#include <linux/dma-mapping.h>
#include <linux/interrupt.h>
#include <linux/clk-provider.h>
#include <linux/clocksource.h>
#include <linux/io.h>
#include <linux/module.h>
#include <linux/of_platform.h>

#include <asm/system_info.h>
#include <asm/mach-types.h>
#include <asm/cputype.h>

#include <asm/mach/arch.h>
#include <asm/mach/map.h>

#include <mach/system.h>

#include <linux/broadcom/vc_cma.h>

/* Effectively we have an IOMMU (ARM<->VideoCore map) that is set up to
 * give us IO access only to 64Mbytes of physical memory (26 bits).  We could
 * represent this window by setting our dmamasks to 26 bits but, in fact
 * we're not going to use addresses outside this range (they're not in real
 * memory) so we don't bother.
 *
 * In the future we might include code to use this IOMMU to remap other
 * physical addresses onto VideoCore memory then the use of 32-bits would be
 * more legitimate.
 */

/* command line parameters */
static unsigned reboot_part = 0;

unsigned long bcm27xx_peri_base __read_mostly;
EXPORT_SYMBOL(bcm27xx_peri_base);
unsigned long bcm27xx_bus_offs __read_mostly;
EXPORT_SYMBOL(bcm27xx_bus_offs);
unsigned long bcm27xx_io_mask __read_mostly;
EXPORT_SYMBOL(bcm27xx_io_mask);
static struct map_desc bcm27xx_io_desc[10] __initdata;

void __init bcm_map_io(unsigned long peri_base, unsigned long bus_offs, unsigned long io_mask)
{
	bcm27xx_peri_base = peri_base;
	bcm27xx_bus_offs = bus_offs;
	bcm27xx_io_mask = io_mask;

	bcm27xx_io_desc[0].virtual = IO_ADDRESS(ARMCTRL_BASE);
	bcm27xx_io_desc[0].pfn = __phys_to_pfn(ARMCTRL_BASE);
	bcm27xx_io_desc[0].length = SZ_4K;
	bcm27xx_io_desc[0].type = MT_DEVICE;
	bcm27xx_io_desc[1].virtual = IO_ADDRESS(UART0_BASE);
	bcm27xx_io_desc[1].pfn = __phys_to_pfn(UART0_BASE);
	bcm27xx_io_desc[1].length = SZ_4K;
	bcm27xx_io_desc[1].type = MT_DEVICE;
	bcm27xx_io_desc[2].virtual = IO_ADDRESS(UART1_BASE);
	bcm27xx_io_desc[2].pfn = __phys_to_pfn(UART1_BASE);
	bcm27xx_io_desc[2].length = SZ_4K;
	bcm27xx_io_desc[2].type = MT_DEVICE;
	bcm27xx_io_desc[3].virtual = IO_ADDRESS(DMA_BASE);
	bcm27xx_io_desc[3].pfn = __phys_to_pfn(DMA_BASE);
	bcm27xx_io_desc[3].length = SZ_4K;
	bcm27xx_io_desc[3].type = MT_DEVICE;
	bcm27xx_io_desc[4].virtual = IO_ADDRESS(MCORE_BASE);
	bcm27xx_io_desc[4].pfn = __phys_to_pfn(MCORE_BASE);
	bcm27xx_io_desc[4].length = SZ_4K;
	bcm27xx_io_desc[4].type = MT_DEVICE;
	bcm27xx_io_desc[5].virtual = IO_ADDRESS(ST_BASE);
	bcm27xx_io_desc[5].pfn = __phys_to_pfn(ST_BASE);
	bcm27xx_io_desc[5].length = SZ_4K;
	bcm27xx_io_desc[5].type = MT_DEVICE;
	bcm27xx_io_desc[6].virtual = IO_ADDRESS(USB_BASE);
	bcm27xx_io_desc[6].pfn = __phys_to_pfn(USB_BASE);
	bcm27xx_io_desc[6].length = SZ_128K;
	bcm27xx_io_desc[6].type = MT_DEVICE;
	bcm27xx_io_desc[7].virtual = IO_ADDRESS(PM_BASE);
	bcm27xx_io_desc[7].pfn = __phys_to_pfn(PM_BASE);
	bcm27xx_io_desc[7].length = SZ_4K;
	bcm27xx_io_desc[7].type = MT_DEVICE;
	bcm27xx_io_desc[8].virtual = IO_ADDRESS(GPIO_BASE);
	bcm27xx_io_desc[8].pfn = __phys_to_pfn(GPIO_BASE);
	bcm27xx_io_desc[8].length = SZ_4K;
	bcm27xx_io_desc[8].type = MT_DEVICE;
	bcm27xx_io_desc[9].virtual = IO_ADDRESS(ARM_LOCAL_BASE);
	bcm27xx_io_desc[9].pfn = __phys_to_pfn(ARM_LOCAL_BASE);
	bcm27xx_io_desc[9].length = SZ_4K;
	bcm27xx_io_desc[9].type = MT_DEVICE;

	iotable_init(bcm27xx_io_desc, ARRAY_SIZE(bcm27xx_io_desc));
}


int calc_rsts(int partition)
{
	return PM_PASSWORD |
		((partition & (1 << 0))  << 0) |
		((partition & (1 << 1))  << 1) |
		((partition & (1 << 2))  << 2) |
		((partition & (1 << 3))  << 3) |
		((partition & (1 << 4))  << 4) |
		((partition & (1 << 5))  << 5);
}

void bcm27xx_restart(enum reboot_mode mode, const char *cmd)
{
	extern char bcm2708_reboot_mode;
	uint32_t pm_rstc, pm_wdog;
	uint32_t timeout = 10;
	uint32_t pm_rsts = 0;

	if(bcm2708_reboot_mode == 'q')
	{
		// NOOBS < 1.3 booting with reboot=q
		pm_rsts = readl(__io_address(PM_RSTS));
		pm_rsts = PM_PASSWORD | pm_rsts | PM_RSTS_HADWRQ_SET;
	}
	else if(bcm2708_reboot_mode == 'p')
	{
		// NOOBS < 1.3 halting
		pm_rsts = readl(__io_address(PM_RSTS));
		pm_rsts = PM_PASSWORD | pm_rsts | PM_RSTS_HADWRH_SET;
	}
	else
	{
		pm_rsts = calc_rsts(reboot_part);
	}

	writel(pm_rsts, __io_address(PM_RSTS));

	/* Setup watchdog for reset */
	pm_rstc = readl(__io_address(PM_RSTC));

	pm_wdog = PM_PASSWORD | (timeout & PM_WDOG_TIME_SET); // watchdog timer = timer clock / 16; need password (31:16) + value (11:0)
	pm_rstc = PM_PASSWORD | (pm_rstc & PM_RSTC_WRCFG_CLR) | PM_RSTC_WRCFG_FULL_RESET;

	writel(pm_wdog, __io_address(PM_WDOG));
	writel(pm_rstc, __io_address(PM_RSTC));
}


/* We can't really power off, but if we do the normal
 * reset scheme, and indicate to bootcode.bin not to
 * reboot, then most of the chip will be powered off */
static void bcm27xx_power_off(void)
{
	extern char bcm2708_reboot_mode;
	if(bcm2708_reboot_mode == 'q')
	{
		// NOOBS < v1.3
		bcm27xx_restart('p', "");
	}
	else
	{
		/* partition 63 is special code for HALT the bootloader knows not to boot*/
		reboot_part = 63;
		/* continue with normal reset mechanism */
		bcm27xx_restart(0, "");
	}
}


static void __init bcm27xx_init_uart1(void)
{
	struct device_node *np;

	np = of_find_compatible_node(NULL, NULL, "brcm,bcm2835-aux-uart");
	if (of_device_is_available(np)) {
		pr_info("bcm27xx: Mini UART enabled\n");
		writel(1, __io_address(UART1_BASE + 0x4));
	}
}


void __init bcm27xx_init(unsigned int boardrev, unsigned int serial, unsigned int reboot_mode)
{
	int ret;

	vc_cma_early_init();

	pm_power_off = bcm27xx_power_off;

	ret = of_platform_populate(NULL, of_default_bus_match_table, NULL,
				   NULL);
	if (ret) {
		pr_err("of_platform_populate failed: %d\n", ret);
		BUG();
	}

	bcm27xx_init_uart1();

	system_rev = boardrev;
	system_serial_low = serial;
	reboot_part = reboot_mode;
}


void __init bcm27xx_timer_init(void)
{
	// timer control
	writel(0, __io_address(ARM_LOCAL_CONTROL));
	// timer pre_scaler
	writel(0x80000000, __io_address(ARM_LOCAL_PRESCALER)); // 19.2MHz
	//writel(0x06AAAAAB, __io_address(ARM_LOCAL_PRESCALER)); // 1MHz

	of_clk_init(NULL);
	clocksource_probe();
}


void __init bcm27xx_init_early(void)
{

	/*
	 * Some devices allocate their coherent buffers from atomic
	 * context. Increase size of atomic coherent pool to make sure such
	 * the allocations won't fail.
	 */
	init_dma_coherent_pool_size(SZ_4M);
}

void __init bcm27xx_board_reserve(void)
{
	vc_cma_reserve();
}


#ifdef CONFIG_SMP
#include <linux/smp.h>

#include <asm/cacheflush.h>
#include <asm/smp_plat.h>

static void bcm2835_send_doorbell(const struct cpumask *mask, unsigned int irq)
{
        int cpu;
        /*
         * Ensure that stores to Normal memory are visible to the
         * other CPUs before issuing the IPI.
         */
        dsb();

        /* Convert our logical CPU mask into a physical one. */
        for_each_cpu(cpu, mask)
	{
		/* submit softirq */
		writel(1 << irq, __io_address(ARM_LOCAL_MAILBOX0_SET0 + 0x10 *
			MPIDR_AFFINITY_LEVEL(cpu_logical_map(cpu), 0)));
	}
}


void __init bcm27xx_smp_init_cpus(void)
{
	void secondary_startup(void);
	unsigned int i, ncores;

	ncores = 4; // xxx scu_get_core_count(NULL);
	printk("[%s] enter (%x->%x)\n", __FUNCTION__,
		(unsigned) virt_to_phys((void *)secondary_startup),
		(unsigned)__io_address(ST_BASE + 0x10));
	printk("[%s] ncores=%d\n", __FUNCTION__, ncores);

	for (i = 0; i < ncores; i++) {
		set_cpu_possible(i, true);
		/* enable IRQ (not FIQ) */
		writel(0x1, __io_address(ARM_LOCAL_MAILBOX_INT_CONTROL0 + 0x4 * i));
		//writel(0xf, __io_address(ARM_LOCAL_TIMER_INT_CONTROL0   + 0x4 * i));
	}
	set_smp_cross_call(bcm2835_send_doorbell);
}


/*
 * for arch/arm/kernel/smp.c:smp_prepare_cpus(unsigned int max_cpus)
 */
void __init bcm27xx_smp_prepare_cpus(unsigned int max_cpus)
{
    printk("[%s] enter\n", __FUNCTION__);
}


/*
 * for linux/arch/arm/kernel/smp.c:secondary_start_kernel(void)
 */
void __init bcm27xx_secondary_init(unsigned int cpu)
{
    printk("[%s] enter cpu:%d\n", __FUNCTION__, cpu);
}


/*
 * for linux/arch/arm/kernel/smp.c:__cpu_up(..)
 */
int __init bcm27xx_boot_secondary(unsigned int cpu, struct task_struct *idle)
{
    void secondary_startup(void);
    void *mbox_set = __io_address(ARM_LOCAL_MAILBOX3_SET0 + 0x10 * MPIDR_AFFINITY_LEVEL(cpu_logical_map(cpu), 0));
    void *mbox_clr = __io_address(ARM_LOCAL_MAILBOX3_CLR0 + 0x10 * MPIDR_AFFINITY_LEVEL(cpu_logical_map(cpu), 0));
    unsigned secondary_boot = (unsigned) virt_to_phys((void *)secondary_startup);
    int timeout=20;
    unsigned t = -1;

    dsb();
    BUG_ON(readl(mbox_clr) != 0);
    writel(secondary_boot, mbox_set);

    while (--timeout > 0) {
	t = readl(mbox_clr);
	if (t == 0) break;
	cpu_relax();
    }
    if (timeout==0)
        printk("[%s] cpu:%d failed to start (%x)\n", __FUNCTION__, cpu, t);
    else
        printk("[%s] cpu:%d started (%x) %d\n", __FUNCTION__, cpu, t, timeout);

    return 0;
}


struct smp_operations  bcm27xx_smp_ops __initdata = {
	.smp_init_cpus		= bcm27xx_smp_init_cpus,
	.smp_prepare_cpus	= bcm27xx_smp_prepare_cpus,
	.smp_secondary_init	= bcm27xx_secondary_init,
	.smp_boot_secondary	= bcm27xx_boot_secondary,
};
#endif

