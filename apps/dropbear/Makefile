
# Nard Linux SDK
# http://www.arbetsmyra.dyndns.org/nard
# Copyright (C) 2014-2016 Ronny Nilsson


include ../../Rules.mk															# Common used stuff

# Dropbear needs the zlib library, so
# including Rules (paths) to it.
include $(PATH_APPS)/z-lib/Rules.mk


#-----------------------------													# Standard targets
.PHONY: all install $(PKG_VER)
all: $(PATH_FS)/root/.ssh/authorized_keys $(PATH_FS)/usr/sbin/$(PKG_NAME)
install: $(PATH_FS)/root/.ssh/authorized_keys $(PATH_FS)/usr/sbin/$(PKG_NAME)
$(PKG_VER): $(PATH_FS)/root/.ssh/authorized_keys $(PATH_FS)/usr/sbin/$(PKG_NAME)


$(PATH_FS)/root/.ssh/authorized_keys: force
$(PATH_FS)/root/.ssh/authorized_keys: $(PATH_FS)/usr/sbin/$(PKG_NAME)
	@# Create a ssh key for your target so that you can log in easily			\
	# during deveopment. Run "make NARD_RELEASE=1 <product>" to skip this		\
	# step. If you have a default key you always want to ship with your			\
	# product, put the _public_ part in:										\
	# platform/<product>/fs-template/root/.ssh/authorized_keys					\
	# Each device may as well have unique login keys in							\
	# /boot/settings/root_ssh_authorized_keys									\
	# which is preserved during upgrades.
	if test -z "$$NARD_RELEASE"; then											\
		if test ! -e "$(PATH_INTER)/product_ssh_key"; then						\
			"$(SSH_KEYGEN)" -t rsa -v -N "" -f "$(PATH_INTER)/product_ssh_key";	\
		fi;																		\
		if test ! -e "$@"; then													\
			install -m 0600 /dev/null "$@";										\
		fi;																		\
		if ! grep -qFf "$(PATH_INTER)/product_ssh_key.pub" "$@"; then			\
			cat "$(PATH_INTER)/product_ssh_key.pub" >>"$@";						\
		fi;																		\
	fi


$(PATH_FS)/usr/sbin/$(PKG_NAME): $(PKG_VER)/.nard-build
	install -m 0755 -d "$(dir $@)"
	install -m 0755 -d "$(PATH_FS)/usr/bin"
	$(CP) -uavf "$(PKG_NAME)/dropbearmulti" \
		"$(PKG_NAME)/dropbear" "$(PKG_NAME)/dropbearkey" "$(dir $@)"
	cd "$(PATH_FS)/usr/bin" && ln -sfv ../sbin/dropbearmulti scp
	install -m 0700 -d "$(PATH_FS)/root/.ssh"
	install -m 0755 -d "$(PATH_FS)/etc/dropbear"
	touch "$@"


$(PKG_VER)/.nard-build: $(std-deps)
	"$(PATH_UTIL)/bin/make_patch_links.sh" "$(PKG_VER)" $($(PKG_VER).patches)
	cd "$(dir $@)" && ./configure CFLAGS="$(CROSS_CFLAGS)"						\
		LDFLAGS="$(CROSS_LDFLAGS)" --host=$(CROSS_TUPLE)						\
		--target=$(CROSS_TUPLE) --disable-pam									\
		--with-zlib=$(PATH_ZLIB)
	$(MAKE) -C "$(PKG_VER)" -j $(CPUS)											\
		PROGRAMS="dropbear scp dropbearkey" MULTI=1 SCPPROGRESS=1
	touch "$@"


$(PKG_VER)/.nard-extract: $(PKG_VER).tar.*
	$(std-extract)



#----------------------------													# Cleaning	
.PHONY: clean		
clean:
	$(std-clean)

.PHONY: distclean
distclean:
	$(std-distclean)

