
# Nard Linux SDK
# http://www.arbetsmyra.dyndns.org/nard
# Copyright (C) 2014-2017 Ronny Nilsson


# -------------------------------------------------------------
# Global variables
pathSdCard="/dev/mmcblk0"



# -------------------------------------------------------------
# Disable hotpluging
hotplug_disable() {
	local pathHp hp

	pathHp="/proc/sys/kernel/hotplug"
	[ -r "$pathHp" ] || return 0

	read -t 5 -s hp <"$pathHp"
	while [ -n "$hp" ]; do
		flock -x /var/lock/hotplug echo >"$pathHp"
		read -t 5 -s hp <"$pathHp"
	done
}



# -------------------------------------------------------------
# Poor mans dynamic cpu offline. Raspberry Pi lack cpu hotplug
# support and power management. Instead we move all processes
# to cpu 0, which will make the remaining cores to idle and
# thus reduce power consumption.
# https://www.raspberrypi.org/forums/viewtopic.php?t=99372#p693896
become_uniprocessor() {
	local a pid

	[ -x "/usr/bin/taskset" ] || return 0

	for f in /proc/[0-9]*/status; do
		a=${f%/*}
		pid=${a##/proc/}
		[ -d "/proc/${pid}" ] && /usr/bin/taskset -p 1 $pid >/dev/null 2>&1		# Ignore errors, best effort
	done
}



# -------------------------------------------------------------
# Reset USB peripherals
reset_usb() {
	local f

	[ -d "/sys/devices/platform/soc" ] || return 0

	for f in /sys/devices/platform/soc/*usb/buspower; do
		[ -w "${f}" ] || continue
		echo 0 >"${f}"
	done
}



# -------------------------------------------------------------
# Test that the power supply has enough juice by loading
# the system to the max for a short period while we
# simultaneously monitor the brownout sensor.
psu_test() {
	if [ -x "/usr/sbin/rpiburn" ] && ! "/usr/sbin/rpiburn"; then
		echo "Error; bad power supply"
		return 1
	fi

	return 0
}



# -------------------------------------------------------------
# Write out all SD card disk buffers and clear the page cache
flush_sdcard() {
	local B

	/bin/sync
	echo 1 >/proc/sys/vm/drop_caches

	for B in ${pathSdCard}*; do
		if [ -b "$B" ]; then
			/bin/fsync "$B"
			/sbin/blockdev --flushbufs "$B"
		fi
	done

	/bin/sleep 1

	return 0
}



# -------------------------------------------------------------
# Returns true if there is a SD card device.
has_Sd_card() {
	test -b "$pathSdCard"
}



# -------------------------------------------------------------
# Returns true if the SD card MBR is in native Nard format.
# (As an alternative it can be user customized.)
has_nard_mbr() {
	has_Sd_card && dd if="$pathSdCard" bs=1 count=4 \
		skip=440 2>/dev/null | strings -n 2 | grep -qi "NARD"
}
