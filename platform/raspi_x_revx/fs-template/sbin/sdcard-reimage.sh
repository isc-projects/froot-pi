#!/bin/ash

# Script for warm reset of the SD card with a
# subsequent full image rewrite. This is as
# close to physically removing the card and
# reformating it from scratch as we can get
# with software only.
#
# Pros:
# - Should work even with low quality and
#   Friday afternoon SD cards.
# - Corrupt SDs will like come back to life
#   (although corruption may soon reappear).
# Cons:
# - Very sensetive to power cuts in the middle
#   of the process.
# - Wipes all user data.
#
# Nard Linux SDK
# http://www.arbetsmyra.dyndns.org/nard
# Copyright (C) 2014-2018 Ronny Nilsson




# -------------------------------------------------------------
# Empty for avoiding error. Is defined in functions.sh.
become_uniprocessor() { return; }



#==============================================================
PATH=${PATH}:/bin:/usr/bin:/sbin:/usr/sbin:/usr/local/bin						# Has had problems with missing paths


if [ $# -eq 0 -o "$1" = "-h" -o "$1" = "--help" ]; then
	echo
	echo "Should the SD card be erased and re-initialized"
	echo "with a new default image? Warning, you will loose"
	echo "ALL data! Only issue this operation if the SD"
	echo "card has become corrupted! Basic Nard settings,"
	echo "such as IP address etc in /boot/settings/ are"
	echo "preserved if possible."
	echo "Usage: $0 --force [FILE]"
	exit 1
elif [ "$1" != "--force" ]; then
	echo "This is a dangerous script, you need"
	echo "to provide the --force option!"
	exit 1
elif [ ! -s "$2" ]; then
	echo "Provide a path to a nard-sdcard.img.zip image file!"
	exit 1;
elif [ $(id -u) -ne 0 ]; then
	echo "You need to root!"
	exit 1
fi


# We are not timing critical
renice 5 $$


# Pull library functions
[ -r "/lib/nard/functions.sh" ] && source "/lib/nard/functions.sh"


# Test that the power supply has enough juice by loading
# the system to the max for a short period while we
# simultaneously monitor the brownout sensor.
if [ -x "/usr/sbin/rpiburn" ] && ! "/usr/sbin/rpiburn"; then
	echo "Error; bad power supply"
	exit 1
fi


# Poor mans dynamic cpu offline. Raspberry Pi lack cpu hotplug
# support and power management. Instead we move all processes
# to cpu 0, which will make the remaining cores to idle and thus
# reduce board power consumption. Hopefully this will leave
# enough power left for the SD card which will need to work hard.
become_uniprocessor



# Backup Nard settings to be able to restore them later
# (if possible, it might fail if filesystem is corrupt).
if ! cp -af "/boot/settings" "/tmp"; then
	echo "Warning, can't preserve settings!"
	sleep 4
	# Do NOT exit script here. It's a warning, not an error.
fi


echo -n "Forcing unmount of all SD card partitions..."
sync
killall -q -USR1 automount && sleep 1 && killall -q automount
echo -n "."
umount -f  /mnt/sdcard/* 2>/dev/null
echo -n "."
echo 1 >/proc/sys/vm/drop_caches
usleep 100000
echo -n "."
for B in /dev/mmcblk*; do
	if [ -b "$B" ]; then
		fsync "$B"
		blockdev --flushbufs "$B"
		echo -n "."
	fi
done
sleep 1
echo " OK"


# Lookup kernel names of MMC and SD peripherals
mmcDevPath="/sys/bus/mmc/drivers/mmcblk"
[ -d "$mmcDevPath" ] || {
	echo "No such directory $mmcDevPath"
	exit 1
}
mmcDevice=$(cd "${mmcDevPath}" && find * -maxdepth 1 -type l)
sdDevPath=$(realpath "${mmcDevPath}"/mmc0*/../device/driver/)
[ -d "$sdDevPath" ] || {
	echo "No such directory $sdDevPath"
	exit 1
}
sdDevice=$(cd "${sdDevPath}" && find * -maxdepth 1 -type l)


# Debug print card attributes
read -t 5 -s mmcType <"${mmcDevPath}/${mmcDevice}/type"
read -t 5 -s mmcName <"${mmcDevPath}/${mmcDevice}/name"
read -t 5 -s mmcCid <"${mmcDevPath}/${mmcDevice}/cid"
echo "Card info: ${mmcType} ${mmcName} ${mmcCid}"
boardRev=$(grep -i "^revision" /proc/cpuinfo | head -n 1 | \
	grep -Eo "[[:xdigit:]]+$")
echo "Host info: ${mmcDevice}, ${sdDevice}, ${boardRev} board"
echo -n "Creating reset..."


# Remove MMC block device
[ -d "${mmcDevPath}/${mmcDevice}" ] && \
	echo "${mmcDevice}" >"${mmcDevPath}/unbind"
while [ -b "/dev/mmcblk0" ]; do
	sleep 1
	echo -n "."
done
sleep 1
echo -n "."


# Remove SD peripheral
[ -d "${sdDevPath}/${sdDevice}" ] && \
	echo "${sdDevice}" >"${sdDevPath}/unbind"
while [ -b "/dev/mmcblk0" ]; do
	sleep 1
	echo -n "."
done
sleep 1
echo -n "."
sleep 1
echo -n "."
sleep 1
echo -n "."
sleep 1
echo -n "."
echo 1 >/proc/sys/vm/drop_caches


# Re-add SD peripheral
[ -e "${sdDevPath}/${sdDevice}" ] || {
	echo "${sdDevice}" >"${sdDevPath}/bind"
	sleep 1
	echo -n "."
	sleep 1
	echo -n "."
	sleep 1
	echo -n "."
}


# Re-add MMC block device
[ -e "${mmcDevPath}/${mmcDevice}" ] || {
	echo "${mmcDevice}" >"${mmcDevPath}/bind"
	sleep 1
	echo -n "."
}


# Wait for kernel to parse partition table if such exist
while [ ! -b "/dev/mmcblk0" ]; do
	sleep 1
	echo -n "."
done
sleep 1
echo " OK"


# Slow down communication with the SD peripheral to
# increase robustness. We will write lots of data soon.
if [ -f "/sys/kernel/debug/mmc0/clock" ]; then
	read -t 5 -s rate <"/sys/kernel/debug/mmc0/clock"
	[ $rate -gt 16000000 ] && echo 16000000 >"/sys/kernel/debug/mmc0/clock"
	usleep 100000
fi


echo -n "Writing image to SD card... "
if echo "$2" | grep -qE "\.img\.zip$"; then										# It's a zip compressed image
	if ! unzip -qp "$2" | cat >"/dev/mmcblk0"; then								# Use cat for buffered writes
		echo -ne "\nUpgrade aborted. Warning, do NOT reboot "
		echo "until you've fixed the problem manually!"
		exit 2
	fi		
elif echo "$2" | grep -qE "\.img\.gz$"; then									# It's a gzip compressed image
	if ! gunzip -t "$2"; then
		echo -e "\nError, broken gzip file!"
		exit 2
	elif ! zcat "$2" | cat >"/dev/mmcblk0"; then								# Use cat for buffered writes
		echo -ne "\nUpgrade aborted. Warning, do NOT reboot "
		echo "until you've fixed the problem manually!"
		exit 2
	fi		
elif ! cat "$2" >"/dev/mmcblk0"; then											# It's a raw image
	echo -ne "\nUpgrade aborted. Warning, do NOT reboot "
	echo "until you've fixed the problem manually!"
	exit 2
fi
echo -n "."
sync
echo 1 >/proc/sys/vm/drop_caches
echo -n "."
usleep 100000
fsync "/dev/mmcblk0"
blockdev --flushbufs "/dev/mmcblk0"
blockdev --rereadpt "/dev/mmcblk0"
echo "OK"


echo -n "Verifying written image... "
if dd if="/dev/mmcblk0" bs=512 count=1 2>/dev/null | \
		strings | grep -qi "NARD"; then
	echo "OK"
else
	echo -e "\nError, invalid MBR!"
	exit 3
fi


echo "Expanding partitions to use the entire SD card..."
sdcard-alloc-rem.sh || exit


# Check that the SD card filesystems are OK
echo "Checking filesystem integrity..."
fsck -ATV -a 2>&1 | while read l; do echo "    $l"; done						# Indent fsck output with some spaces


echo "Mounting filesystems..."
/usr/sbin/automount -p /var/run/automount.pid -n 2


if test -d "/tmp/settings"; then
	echo -n "Restoring basic settings on top of new image... "
	cp -af "/tmp/settings" "/boot"
	echo "OK"
fi

sync

