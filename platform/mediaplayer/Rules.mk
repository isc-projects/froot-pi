
#-------------------------------------------
# Product "mediaplayer" recipe
#-------------------------------------------


# List of applications this product needs. Packages
# will be built in the listed order (unless you
# explicitly define the dependencies).
PKGS_APPS += salsa-lib/salsa-lib-0.1.3
PKGS_APPS += mpg123/mpg123-1.21.0
PKGS_APPS += alsa-utils/alsa-utils-1.0.25
PKGS_APPS += jpeg-lib/jpeg-lib-8d
PKGS_APPS += fbi/fbi-1.31


# List of application patches this platform needs.
fbi-1.31.patches += fbtools.c
fbi-1.31.patches += fbi.c
fbi-1.31.patches += fb-gui.c
fbi-1.31.patches += fb-gui.h


# Explicit package dependencies (optional, but
# nice to have for librarys)
mpg123/mpg123-1.21.0: salsa-lib/salsa-lib-0.1.3
alsa-utils/alsa-utils-1.0.25: salsa-lib/salsa-lib-0.1.3



#-----------------------------
# Get defines which tell what hw and
# sw we use and thus need (if any).
PRODUCT_DEPS += skeleton
include $(PATH_TOP)/platform/skeleton/Rules.mk

