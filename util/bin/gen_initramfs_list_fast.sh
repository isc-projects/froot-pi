#!/bin/bash

# Generate an input file for the gen_init_cpio tool
# shiped with the kernel. This replaces the gen_initramfs_list.sh
# tool (shiped with the kernel as well) which is slooow.
#
# Nard Linux SDK
# http://www.arbetsmyra.dyndns.org/nard
# Copyright (C) 2014,2015 Ronny Nilsson


set -e

uid=$(id -u)
gid=$(id -g)

# Map current user to root if we aren't root already
if [[ $uid -eq 0 ]] || [[ $gid -eq 0 ]]; then
    sedOpts=""
else
    sedOpts="s/$uid $gid$/0 0/"
fi


# Was a directory supplied on the command line?
if [[ ! -d "$1" ]] || [[ -z "$2" ]]; then
    echo "Usage: $0 <virtual rootfs-tree> <real rootfs-tree>"
    exit 1
fi


# Generate the output in desired format
find -P "$1" $extraFindOpts -type d -printf "dir /%P %m %U %G\n" | 
    sed -e "$sedOpts"

find -P "$1" $extraFindOpts -type p -printf "pipe /%P %m %U %G\n" | 
    sed -e "$sedOpts"

find -P "$1" $extraFindOpts -type s -printf "sock /%P %m %U %G\n" | 
    sed -e "$sedOpts"

find -P "$1" $extraFindOpts -type f -printf "file /%P $2/%P %m %U %G\n" | 
    sed -e "$sedOpts"

find -P "$1" $extraFindOpts -type l -printf "slink /%P %l %m %U %G\n" | 
    sed -e "$sedOpts"


# Search for device files (with major and minor numbers).
# The find utility can't -printf those so we currently
# doesn't support it. Usually no big deal in Nard though.
devFiles=$(find -P "$1" $extraFindOpts \( -type b -o -type c \))
if [[ -n "$devFiles" ]]; then
    echo -n "Error, support for device special files has been" >&2
    echo " optimized away in favour of build speed. Can't use" >&2
    echo "$devFiles" >&2
    exit 1
fi

