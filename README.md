This is a fork of the Nard SDK to support building ISC's fast-root
server for the Raspberry Pi.

Building from source requires at least 20GB of disk space.
Pre-compiled binaries  will be made available soon.

http://www.arbetsmyra.dyndns.org/nard/

https://github.com/isc-projects/froot-src
